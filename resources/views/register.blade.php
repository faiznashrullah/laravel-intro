<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Form Pendaftaran</title>
</head>
<body>
	<form action="/welcome" method="POST">
		@csrf
		<div class="header">
			
			<h1>Buat Account Baru!</h1>
			<h2>Sign Up Form</h2>

		</div>

		<div class="name">
			
			<label for="fname">First name:</label> <br><br>

			<input type="text" name="first_name" id="fname"> <br><br>

			<label for="lname">Last name:</label> <br><br>

			<input type="text" name="last_name" id="lname">

		</div>

		<div class="gender">
			
			<p>Gender:</p>

			<input type="radio" id="pr" name="gender" value="male"><label for="pr">Male</label> <br>

			<input type="radio" id="wn" name="gender" value="female"><label for="wn">Female</label> <br>

			<input type="radio" id="ot" name="gender" value="other"><label for="ot">Other</label>

		</div>

		<div class="region"></div>

			<p>Nationality:</p>

			<select name="country">
				<option value="id">Indonesian</option>
				<option value="jpn">Japan</option>
				<option value="usa">USA</option>

			</select>

		<div class="language">
			
			<p>Language Spoken:</p>

			<input type="checkbox" id="1" name="language" value="id"><label for="1">Bahasa Indonesia</label> <br>

			<input type="checkbox" id="2" name="language" value="eng"><label for="2">English</label> <br>

			<input type="checkbox" id="3" name="language" value="ot"><label for="3">Other</label>

		</div>

		<div class="bio">
			
			<p>Bio:</p>

			<textarea name="bio" cols="30" rows="10"></textarea>

		</div>

		<input type="submit" value="kirim">

	</form>

	
</body>
</html>