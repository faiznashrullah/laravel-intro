<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
    	return view('register');
    }

    public function welcome(Request $request){
    	$depan=$request["first_name"];
    	$belakang=$request["last_name"];

    	return view('welcome',compact('depan','belakang'));
    }

}

